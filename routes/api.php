<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('tickets')
    ->group(function () {

    // anybody can add ticket
    Route::post('/', 'ApiController@createTicket');

    // but only operators can perform actions with tickets
    Route::middleware(['auth:api', 'operator'])->group(function () {
        Route::post('/{id}/take', 'ApiController@takeTicket');
        Route::post('/{id}/close', 'ApiController@closeTicket');
    });
});
