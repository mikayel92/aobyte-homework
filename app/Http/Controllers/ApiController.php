<?php

namespace App\Http\Controllers;

use Auth;
use App\Events\TicketAssigned;
use App\Http\Requests\CreateTicket;
use App\Ticket;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Function to create new tickets.
     *
     * @param CreateTicket $request
     *
     * @return JsonResponse
     */
    public function createTicket(CreateTicket $request): JsonResponse
    {
        $ticket = new Ticket($request->validated());
        $ticket->save();

        return response()->json([
            'message' => 'Successfully created!'
        ]);
    }

    /**
     * Function to assign ticket to user.
     *
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function takeTicket(Request $request, $id): JsonResponse
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->user_id = Auth::user()->id;

        DB::transaction(function () use ($ticket) {

            $ticket->save();
            // to not listen event fired by himself
            broadcast(new TicketAssigned($ticket->id))->toOthers();
        });

        return response()->json([
            'message' => 'Successfully assigned!'
        ]);
    }

    /**
     * Function to close ticket.
     *
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function closeTicket(Request $request, $id): JsonResponse
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->status = Ticket::STATUS_CLOSED;

        $ticket->save();

        return response()->json([
            'message' => 'Successfully closed!'
        ]);
    }
}
