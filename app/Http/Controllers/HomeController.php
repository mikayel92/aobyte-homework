<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('operator')->only('showTicketsDashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show tickets dashboard.
     *
     * @return Renderable
     */
    public function showTicketsDashboard(): Renderable
    {
        $actualTickets = Ticket::actual()
            ->get();

        $userTickets = Auth::user()->tickets;

        return view('tickets')
            ->with([
                'actualTickets' => $actualTickets,
                'userTickets'   => $userTickets
            ]);
    }
}
