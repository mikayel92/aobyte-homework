<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TicketCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id, $title, $content;

    /**
     * Create a new event instance.
     *
     * @param mixed ...$params
     *
     * @return void
     */
    public function __construct(...$params)
    {
        list($this->id, $this->title, $this->content) = $params;
    }

    /**
     * Events broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'ticket.created';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('tickets-channel');
    }
}
