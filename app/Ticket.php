<?php

namespace App;

use App\Events\TicketCreated;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public const
        STATUS_OPEN = 'OPEN',
        STATUS_CLOSED = 'CLOSED';

    /**
     * Ticket constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        static::created(function () {
            event(new TicketCreated($this->id, $this->title, $this->content));
        });

        parent::__construct($attributes);
    }

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOpen(Builder $query): Builder
    {

        return $query->where('status', self::STATUS_OPEN);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeWithoutOperator(Builder $query): Builder
    {
        return $query->where('user_id', 0);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeActual(Builder $query): Builder
    {
        return $query->open()->withoutOperator();
    }

    /**
     * Function to check ticket is open.
     *
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->status === self::STATUS_OPEN;
    }
}
