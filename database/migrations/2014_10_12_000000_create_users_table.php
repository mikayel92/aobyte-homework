<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token', 80)->unique()
                ->nullable()
                ->default(null);
            $table->enum('role', [
                User::ROLE_ADMIN,
                USER::ROLE_OPERATOR
            ])->default(User::ROLE_OPERATOR);
            $table->rememberToken();
            $table->timestamps();
        });

        // Setting first user as admin (super admin),
        // that wil have more privileges than operators.
        // For example, creating operators.
        // There was no info about implementing roles and permissions logic
        // for this task. So just adding this user in any case.
        DB::table('users')->insert([
                [
                    'name'      => 'Super Admin',
                    'email'     => 'superadmin@exmaple.com',
                    'password'  => bcrypt('Password1234'),
                    'api_token' => Str::random(60),
                    'role'      => User::ROLE_ADMIN,
                ],
                // adding 2 operators to for tickets testing to not create manually
                [
                    'name'      => 'Operator 1',
                    'email'     => 'operator1@exmaple.com',
                    'password'  => bcrypt('Password1234'),
                    'api_token' => Str::random(60),
                    'role'      => User::ROLE_OPERATOR,
                ], [
                    'name'      => 'Operator 2',
                    'email'     => 'operator2@exmaple.com',
                    'password'  => bcrypt('Password1234'),
                    'api_token' => Str::random(60),
                    'role'      => User::ROLE_OPERATOR
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
