<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tickets')->truncate();

        $faker = Factory::create();

        $data = [];
        for ($i = 0; $i < 10; $i ++) {
            $data []= [
                'title'      => $faker->words(5, true),
                'content'    => $faker->text,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        DB::table('tickets')->insert($data);
    }
}
