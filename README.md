##To setup and test project

After cloning from git.

1. run `composer install -v`. I pushed `composer.lock` file so you have same versions of packages.
2. copy `env.example` to `.env`
3. set Mysql variables in `env`
4. run command `php artisan migrate --seed`
5. Test with users created in user migration

