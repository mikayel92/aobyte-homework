import Echo from "laravel-echo";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

let headers = {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    },
    $authTokenMeta = $('meta[name="auth-token"]');

if ($authTokenMeta.length) {
    headers.Authorization = 'Bearer ' + $authTokenMeta.attr('content');
}
$.ajaxSetup({
    headers: headers
});

let $script = {

    _events : function() {

        $('body').on('click', 'a.with-ajax', function (e) {

            e.preventDefault();

            let $this  = $(this),
                url    = $this.attr('href'),
                method = $this.data('method');

            if (method === undefined)
                method = 'get';

            $.ajax({
                url : url,
                method : method,
                success : function(result) {

                    if ($this.hasClass('take-url')) {

                        let td = $this.parent(), //getting parent
                            tr = td.parent(); // getting row

                        td.html(`<a class="btn btn-success with-ajax close-url" data-method="post" href="/api/tickets/${$this.data('id')}/close">Close</a>`)

                        $('#my-tickets-table tbody').append(`<tr>${tr.html()}</tr>`);

                        tr.remove();
                    }

                    if ($this.hasClass('close-url')) {

                        $this.replaceWith('<label>Closed</label>')
                    }
                }
            });
        });

        $('form.with-ajax').on('submit', function (e) {

            e.preventDefault();

            let $this           = $(this),
                url             = $this.attr('action'),
                method          = $this.attr('method'),
                data            = $this.serializeArray(),
                $successMessage = $this.find('.success-message'),
                $errorMessages  = $this.find('.error-message');

            // emptying all prev messages
            $errorMessages.html('');
            $successMessage.html('');

            $.ajax({
                url : url,
                method : method,
                data : data,
                success : function(result) {
                    $successMessage.html(result.message);
                    $this.find('input, textarea').val('').html('');
                },
                error : function (result) {

                  $.each(result.responseJSON.errors, function (index, value) {

                      let $current = $(`[name="${index}"]`);

                      $current.next().html(value);
                  });
                }
            });
        });

        window.Echo.private('tickets-channel')
            .listen('.ticket.created', (data) => { // event name need to be prefixed with a dot

                let $openTicketsTable = $('#open-tickets-table');

                if ($openTicketsTable.length) {

                    let ticketHtml = `<tr style="background-color: green" id="open-item-${data.id}">
                        <td>${data.id}</td>
                        <td>${data.title}</td>
                        <td>${data.content}</td>
                        <td>
                            <a class="btn btn-light with-ajax" data-method="post" href="/api/tickets/${data.id}/take">Take</a>
                        </td>
                    </tr>`;

                    $openTicketsTable.find('tr').last().after(ticketHtml);
                }
            })
            .listen('.ticket.assigned', (data) => { // event name need to be prefixed with a dot

                let $openTicketsTable = $('#open-tickets-table');

                if ($openTicketsTable.length) {

                    $('#open-item-' + data.id).remove();
                }
            });
    },

    init : function () {
        $script._events();
    }
};

$script.init();
