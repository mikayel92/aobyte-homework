@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">Open tickets</div>

                <div class="card-body">
                    <table class="table" id="open-tickets-table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Content</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($actualTickets as $actualTicket)
                            <tr id="open-item-{{$actualTicket->id}}">
                                <td>{{$actualTicket->id}}</td>
                                <td>{{$actualTicket->title}}</td>
                                <td>{{$actualTicket->content}}</td>
                                <td>
                                    <a data-id="{{$actualTicket->id}}"
                                       class="btn btn-light with-ajax take-url"
                                       data-method="post"
                                       href="/api/tickets/{{$actualTicket->id}}/take"
                                    >
                                        Take
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">My tickets</div>

                <div class="card-body">
                    <table class="table" id="my-tickets-table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Content</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userTickets as $userTicket)
                            <tr id="open-item-{{$userTicket->id}}">
                                <td>{{$userTicket->id}}</td>
                                <td>{{$userTicket->title}}</td>
                                <td>{{$userTicket->content}}</td>
                                <td class="text-center">
                                    @if($userTicket->isOpen())
                                    <a class="btn btn-success with-ajax close-url"
                                       data-method="post"
                                       href="/api/tickets/{{$userTicket->id}}/close"
                                    >
                                        Close
                                    </a>
                                    @else
                                    <label>Closed</label>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
